#include <stdio.h>
#include <stdlib.h>

#define COLOR_BOLD "\e[1m"
#define COLOR_ITALIC "\e[3m"
#define COLOR_UNDERLINE "\e[4m"
#define COLOR_OFF "\e[m"
#define CANT_MECHANICS 6
#define CANT_ARCHETYPES 10

const char *mechanics[] = {
    COLOR_BOLD "Disguise:" COLOR_OFF " You may cast this card face down for 3 " 
        "as a 2/2 creature with ward 2. Turn it face up any time for its "
        "disguise cost.",
    COLOR_BOLD "Cloak:" COLOR_OFF " Put it onto the battlefield face down " 
        "as a 2/2 creature with ward 2. Turn it face up any time for its "
        "mana cost if it's a creature card.",
    COLOR_BOLD "Investigate:" COLOR_OFF " Create a colorless clue artifact "
        "token with" COLOR_ITALIC " \"2, Sacrifice this artifact: Draw "
        "a card\"." COLOR_OFF,
    COLOR_BOLD "Suspect:" COLOR_OFF " A suspected creature has menace and "
        "can't block.",
    COLOR_BOLD "Collect evidence N:" COLOR_OFF " Exile cards with total " 
        "mana value N or greater from your graveyard.",
    COLOR_BOLD "Case:" COLOR_OFF " Is an enchantment type that represent "
        "mysteries to solve."
};

const char* archetypes[] = {
    "Azorius Detectives",
    "Dimir Clues Control",
    "Rakdos Suspect Aggro",
    "Gruul Big Disguise",
    "Selesnya Go-wide Disguise",
    "Orzhov Pint-size(Weenie) Disguise",
    "Izzet Artifact Sacrifice",
    "Golgari Gravebreak(Recursion)",
    "Boros Battalion(Attack with at least 3 creatures)",
    "Simic Collect evidence"
};

void menu(int option);
void print_mechanics();
void print_archetypes();

int main() {
    int option = -1;

    do {
        printf(COLOR_BOLD COLOR_UNDERLINE "MAIN MENU\n" COLOR_OFF);
        printf("Enter 1 to view the mechanics\n");
        printf("Enter 2 to view the archetypes\n");
        printf("Enter 0 to exit\n");
        scanf("%d", &option);

        menu(option);
    } while (option);

    return 0;
}

void menu(int option) {
    switch (option) {
        case 0: 
            printf("Exit program\n");
            exit(0);
            break;
        case 1: 
            print_mechanics();
            break;
        case 2:
            print_archetypes();
            break;
    }
}

void print_mechanics() {
    printf(COLOR_UNDERLINE COLOR_BOLD "MECHANICS\n" COLOR_OFF);

    for (int i = 0; i < CANT_MECHANICS; i++) {
        printf("%s\n", mechanics[i]);
    }
    printf("\n");
}

void print_archetypes() {
    printf(COLOR_UNDERLINE COLOR_BOLD "ARCHETYPES\n" COLOR_OFF);

    for (int i = 0; i < CANT_ARCHETYPES; i++) {
        printf("%s\n", archetypes[i]);   
    }
    printf("\n");
}
